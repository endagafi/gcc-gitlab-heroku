# GCC TP-2

Este proyecto es un ejemplo de CI/CD(continuos integration/continuos deployment)

## Funcionamiento

El proyecto esta escrito con el framework web Django, y contiene un archivo .yml
donde se especifican los stages y los enviroment(Development,Staging,Deployment)
diferentes, por los que debe pasar el código del proyecto para finalmente ser 
deployado en Heroku.

### Datos de Prueba

El sistema utiliza un script ubicado en /datos_de_prueba/poblar.sh que precarga 
los siguientes usuarios:

```
    cargar_user('javier', 'Javier', 'Adorno', 'javier@gmail.com')
    cargar_user('ingrid', 'Ingrid', 'López', 'ingrid@gmail.com')
    cargar_user('moises', 'Moisés', 'Cabrera', 'moises@gmail.com')
    cargar_user('jose', 'Jose', 'Palacios', 'jose@gmail.com')
    cargar_user('joel', 'Joel', 'Florentin', 'joel@gmail.com')
    cargar_user('enzo', 'Enzo', 'Galeano', 'enzo@gmail.com')
```

Todos con el password **12345**

### Visualizar Aplicación

La Aplicacion se encuentra en el siguente link:

* [gcc-production-tp](https://gcc-production-tp.herokuapp.com/) - APP EN HEROKU
