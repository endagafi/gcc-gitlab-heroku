from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

def user_login(request):
    if request.user.id is not None:
        return redirect('index')
    if request.method == "POST":
        data = request.POST
        if 'name' in data and 'password' in data:
            user = authenticate(
                username=data['name'], 
                password=data['password']
            )
            if user is not None:
                login(request, user)
                return redirect('index')

    return render(request, 'login.html')

@login_required
def user_logout (request):
    logout(request)
    return redirect('login')

def index(request):
    return render(request,'index.html')

def other(request):
    return redirect('index')

