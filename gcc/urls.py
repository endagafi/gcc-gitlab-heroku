from django.contrib import admin
from django.urls import path,re_path
from django.conf.urls import url
from gcc.views import user_login,user_logout,index,other


urlpatterns = [
    path('',user_login,name="login"),
    path('home/',index,name="index"),
    path('logout/',user_logout),
    re_path(r'.+',other)
]
