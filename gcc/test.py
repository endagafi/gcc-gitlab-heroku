from django.test import TestCase
from django.contrib.auth.models import User

class Test_Trivial(TestCase):

    def setUp(self):
        self.user = User.objects.create_superuser(
            username='gcc',
            email='gcc@gmail.com',
            password='password'
        )

    def test_login(self):
        response = self.client.post("/", data = {
            "name" : "gcc",
            "password": "password"
        })
        self.assertEqual(response.status_code,302)
        self.assertEqual(response.url, "/home/")
        
    def test_logout(self):

        self.client.login(
            username = "gcc",
            password = "password"
        )
        response = self.client.get("/logout/")
        self.assertEqual(response.status_code,302)
        self.assertEqual(response.url, "/")

    def test_home(self):
        response = self.client.get("/home/")
        self.assertEqual(response.status_code, 200)

    def test_other(self):
        response = self.client.get("/other_url/")
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, "/home/")

    
