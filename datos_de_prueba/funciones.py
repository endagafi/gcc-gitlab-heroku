from django.contrib.auth.models import User
PASSWORD = '12345'

# GETTERS


def get_user(username):
    return User.objects.get(username=username)


# CARGAR USERS

def cargar_admin():
    admin = User(username='admin', email='admin@admin.com', is_superuser=True, is_staff=True)
    admin.set_password(PASSWORD)
    admin.save()
    print(
        " + Supersuario '{}' agregado (este usuario es solo para el sitio de administración "
        "de django)".format(admin.username)
    )

def cargar_user(username, first_name, last_name, email):
    u = User(username=username, first_name=first_name, last_name=last_name, email=email)
    u.set_password(PASSWORD)
    u.save()
    print(" + Usuario '{}' agregado".format(u.username))

def cargar_users():
    cargar_admin()
    cargar_user('javier', 'Javier', 'Adorno', 'javier@gmail.com')
    cargar_user('ingrid', 'Ingrid', 'López', 'ingrid@gmail.com')
    cargar_user('moises', 'Moisés', 'Cabrera', 'moises@gmail.com')
    cargar_user('jose', 'Jose', 'Palacios', 'jose@gmail.com')
    cargar_user('joel', 'Joel', 'Florentin', 'joel@gmail.com')
    cargar_user('enzo', 'Enzo', 'Galeano', 'enzo@gmail.com')
