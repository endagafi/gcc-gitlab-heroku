Django==2.0.3
psycopg2-binary==2.7.4
setuptools==40.6.3
gunicorn
django-heroku